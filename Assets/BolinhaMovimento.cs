﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BolinhaMovimento : MonoBehaviour {
	private Rigidbody rb;
	public float velocidade;

	public GameObject itemParticula;

	public Text score;
	public Text textoFinal;

	private int pontos;

	// Use this for initialization
	void Start() {
		rb = GetComponent<Rigidbody>();
		textoFinal.text = ""; 
		//textoFinal.IsActive = false;

		score.text = score.text + pontos.ToString();
	}

	// Update is called once per frame
	void FixedUpdate() { 
		Vector3 movimento = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
		rb.AddForce(movimento * velocidade);
	}

	void OnTriggerEnter(Collider outroObjeto) {
		if(outroObjeto.gameObject.CompareTag("item")) {
			Instantiate(itemParticula, outroObjeto.gameObject.transform.position, Quaternion.identity);
			Destroy(outroObjeto.gameObject); 
			MarcaPonto();
		} 
	}

	void MarcaPonto() {
		pontos++;
		score.text = "Score: " + pontos.ToString();

		if (pontos==8) {
			textoFinal.text = "You Win!";
		}
	}
}