﻿using UnityEngine;
using System.Collections;

public class CameraMovimento : MonoBehaviour {
	public GameObject bolinha;
	private Vector3 posicaoInicial;

	// Use this for initialization
	void Start() {
		posicaoInicial = transform.position - bolinha.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate() {
		transform.position = bolinha.transform.position + posicaoInicial;
	}
}