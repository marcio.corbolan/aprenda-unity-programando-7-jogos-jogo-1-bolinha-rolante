﻿using UnityEngine;
using System.Collections;

public class ObjetoDestruir : MonoBehaviour {
	// Use this for initialization
	void Start() {
		Invoke("ApagaObjeto", 1.5f);
	} 

	void ApagaObjeto() {
		Destroy(this.gameObject);
	}
}